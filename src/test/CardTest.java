package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Rule;
import org.junit.rules.*;
import org.junit.Test;

import main.model.Card;
import main.model.Suit;

public class CardTest {
	
	Card testCard;
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@After
	public void tearDown() throws Exception {
		testCard = null;
	}
	
	@Test
	public void constructorGivenInvalidValue() throws Exception {
		exception.expect(IllegalArgumentException.class);
		testCard = new Card(Suit.DIAMONDS, 20);
	}
	
	@Test
	public void testGetSuit() throws Exception {
		testCard = new Card(Suit.DIAMONDS,4);
		assertEquals(testCard.getSuit(), Suit.DIAMONDS);
	}
	
	@Test
	public void testGestValue() throws Exception {
		testCard = new Card(Suit.HEARTS, 10);
		assertEquals(testCard.getValue(), 10);
	}

}
