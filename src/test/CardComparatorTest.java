package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import main.model.Card;
import main.model.CardComparator;
import main.model.Suit;

public class CardComparatorTest {
	
	CardComparator comparator;
	Card card1, card2;
	
	@Before
	public void setUp() {
		comparator = new CardComparator();
	}
	
	@After
	public void tearDown() {
		card1 = null;
		card2 = null;
	}
	
	@Test
	public void testLessThan() {
		card1 = new Card(Suit.DIAMONDS, 4);
		card2 = new Card(Suit.SPADES, 10);
		assertEquals(comparator.compare(card1, card2), -1);
	}
	
	@Test
	public void testEqual() {
		card1 = new Card(Suit.DIAMONDS, 4);
		card2 = new Card(Suit.HEARTS, 4);
		assertEquals(comparator.compare(card1, card2), 0);
	}
	
	@Test
	public void testGreaterThan() {
		card1 = new Card(Suit.DIAMONDS, 4);
		card2 = new Card(Suit.CLUBS, 2);
		assertEquals(comparator.compare(card1, card2), 1);
	}
	

}
