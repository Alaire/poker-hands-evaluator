package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import main.model.Card;
import main.model.Suit;
import main.model.Hand;
import main.model.HandComparator;

public class HandComparatorTest {
	
	HandComparator comparator;
	Hand hand1, hand2;
	
	@Before
	public void setUp() {
		comparator = new HandComparator();
	}
	
	@After
	public void tearDown() {
		hand1 = null;
		hand2 = null;
	}
	
	@Test
	public void testLessThan() throws Exception {
		Card[] cards1 = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 9)}; //4 of a kind
		hand1 = new Hand(1, cards1);
		Card[] cards2 = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 12), new Card(Suit.SPADES, 8), new Card(Suit.SPADES, 10), new Card(Suit.SPADES, 9)}; //straight flush
		hand2 = new Hand(2, cards2);
		assertEquals(comparator.compare(hand1, hand2), -1);
	}
	
	@Test
	public void testEqual() throws Exception{
		Card[] cards = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 9)};
		hand1 = new Hand(1, cards);
		hand2 = new Hand(2, cards);
		assertEquals(comparator.compare(hand1, hand2), 0);
	}
	
	@Test
	public void testRankTieGreaterThan() throws Exception {
		Card[] cards1 = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 9)};
		hand1 = new Hand(1, cards1);
		Card[] cards2 = new Card[]{new Card(Suit.SPADES, 10), new Card(Suit.SPADES, 10), new Card(Suit.SPADES, 10), new Card(Suit.SPADES, 10), new Card(Suit.SPADES, 9)};
		hand2 = new Hand(2, cards2);
		assertEquals(comparator.compare(hand1, hand2), 1);
	}

	
	@Test
	public void testGreaterThan() throws Exception{
		Card[] cards1 = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 12), new Card(Suit.SPADES, 8), new Card(Suit.SPADES, 10), new Card(Suit.SPADES, 9)}; //straight flush
		hand1 = new Hand(1, cards1);
		Card[] cards2 = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 9)}; //4 of a kind
		hand2 = new Hand(2, cards2);
		assertEquals(comparator.compare(hand1, hand2), 1);
	}

}
