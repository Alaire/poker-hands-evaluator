package test;

import static org.junit.Assert.*;

import org.junit.Test;

import main.model.HandType;

public class HandTypeTest {
	
	@Test
	public void checkHighCardRank() throws Exception {
		assertEquals(HandType.HIGH_CARD.getHandTypeRank(), 0);
	}
	
	@Test
	public void checkPairRank() throws Exception {
		assertEquals(HandType.PAIR.getHandTypeRank(), 1);
	}
	
	@Test
	public void checkTwoPairsRank() throws Exception {
		assertEquals(HandType.TWO_PAIRS.getHandTypeRank(),2);
	}
	
	@Test
	public void checkThreeOfAKindRank() throws Exception {
		assertEquals(HandType.THREE_OF_A_KIND.getHandTypeRank(),3);
	}
	
	@Test
	public void checkStraightRank() throws Exception {
		assertEquals(HandType.STRAIGHT.getHandTypeRank(),4);
	}

	@Test
	public void checkFlushRank() throws Exception {
		assertEquals(HandType.FLUSH.getHandTypeRank(),5);
	}
	
	@Test
	public void checkFullHouseRank() throws Exception {
		assertEquals(HandType.FULL_HOUSE.getHandTypeRank(),6);
	}
	
	@Test
	public void checkFourOfAKindRank() throws Exception {
		assertEquals(HandType.FOUR_OF_A_KIND.getHandTypeRank(),7);
	}
	
	@Test
	public void checkStraightFlushRank() throws Exception {
		assertEquals(HandType.STRAIGHT_FLUSH.getHandTypeRank(),8);
	}
}
