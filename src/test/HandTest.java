package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import main.model.Suit;
import main.model.Card;
import main.model.Hand;
import main.model.HandType;

public class HandTest {
	
	Card[] cards;
	Hand testHand;
	
	@After
	public void tearDown() throws Exception {
		cards = null;
		testHand = null;
	}
	
	@Test
	public void testGetHandType() throws Exception {
		Card [] cards = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.CLUBS, 2), new Card(Suit.HEARTS, 8), new Card(Suit.DIAMONDS, 4), new Card(Suit.SPADES, 5)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.HIGH_CARD);
	}
	
	
	@Test
	public void testGetCards() throws Exception {
		Card[] cards = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.CLUBS, 2), new Card(Suit.HEARTS, 8), new Card(Suit.DIAMONDS, 4), new Card(Suit.SPADES, 5)};
		testHand = new Hand(1, cards);
		assertArrayEquals(testHand.getCards(), new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.HEARTS, 8), new Card(Suit.SPADES, 5), new Card(Suit.DIAMONDS, 4), new Card(Suit.CLUBS, 2)});
		
	}
	
	@Test
	public void testGetID() throws Exception {
		Card [] cards = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.CLUBS, 2), new Card(Suit.HEARTS, 8), new Card(Suit.DIAMONDS, 4), new Card(Suit.SPADES, 5)};
		testHand = new Hand(1,cards);
		assertEquals(testHand.getID(),1);
	}
	
	
	//BUILDER TESTS
	
	@Test
	public void testHandTypeStraightFlush() throws Exception {
		cards = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 12), new Card(Suit.SPADES, 8), new Card(Suit.SPADES, 10), new Card(Suit.SPADES, 9)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.STRAIGHT_FLUSH);
		
	}
	
	@Test
	public void testHandTypeFourOfAKind() throws Exception {
		cards = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 9)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.FOUR_OF_A_KIND);
	}
	
	@Test
	public void testHandTypeFullHouse() throws Exception {
		cards = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 9), new Card(Suit.SPADES, 9)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.FULL_HOUSE);
	}
	
	@Test
	public void testHandTypeFlush() throws Exception {
		cards = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 12), new Card(Suit.SPADES, 13), new Card(Suit.SPADES, 8), new Card(Suit.SPADES, 9)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.FLUSH);
	}
	
	@Test
	public void testHandTypeStraight() throws Exception {
		cards = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.SPADES, 12), new Card(Suit.HEARTS, 8), new Card(Suit.CLUBS, 10), new Card(Suit.SPADES, 9)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.STRAIGHT);
	}
	
	@Test
	public void testHandTypeStraightWithLowAce() throws Exception {
		cards = new Card[]{new Card(Suit.SPADES, 14), new Card(Suit.SPADES, 2), new Card(Suit.HEARTS, 3), new Card(Suit.DIAMONDS, 4), new Card(Suit.SPADES, 5)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.STRAIGHT);
	}
	
	@Test
	public void testHandTypeThreeOfAKind() throws Exception {
		cards = new Card[]{new Card(Suit.SPADES, 14), new Card(Suit.SPADES, 14), new Card(Suit.HEARTS, 14), new Card(Suit.DIAMONDS, 4), new Card(Suit.SPADES, 5)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.THREE_OF_A_KIND);
	}
	
	@Test
	public void testHandTypeTwoPair() throws Exception {
		cards = new Card[]{new Card(Suit.SPADES, 14), new Card(Suit.SPADES, 2), new Card(Suit.HEARTS, 14), new Card(Suit.DIAMONDS, 2), new Card(Suit.SPADES, 5)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.TWO_PAIRS);
	}
	
	@Test
	public void testHandTypePair() throws Exception {
		cards = new Card[]{new Card(Suit.SPADES, 14), new Card(Suit.SPADES, 2), new Card(Suit.HEARTS, 3), new Card(Suit.DIAMONDS, 5), new Card(Suit.SPADES, 5)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.PAIR);
	}
	
	@Test
	public void testHandTypeHighCard() throws Exception {
		cards = new Card[]{new Card(Suit.SPADES, 11), new Card(Suit.CLUBS, 2), new Card(Suit.HEARTS, 8), new Card(Suit.DIAMONDS, 4), new Card(Suit.SPADES, 5)};
		testHand = new Hand(1, cards);
		assertEquals(testHand.getHandType(), HandType.HIGH_CARD);
	}
	

}
