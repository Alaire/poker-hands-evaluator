package main;

import java.util.Scanner;

import main.UserPrompts;
import main.model.Suit;
import main.model.HandComparator;
import main.model.Hand;
import main.model.Card;

public class Main {
	
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		
		UserPrompts.welcome();
		UserPrompts.giveInstructions();
		
		Hand hand1 = receiveHand(1);
		Hand hand2 = receiveHand(2);
		
		getResult(hand1, hand2);
		
		exit();
	}
	
	private static void getResult(Hand hand1, Hand hand2) {
		
		HandComparator comparator = new HandComparator();
		int result = comparator.compare(hand1, hand2);
		
		if (result == -1) {
			UserPrompts.announceWinner(hand2.getID());
		}
		else if (result == 1) {
			UserPrompts.announceWinner(hand1.getID());
		}
		else {
			UserPrompts.announceTie();
		}
		
	}
	
	private static Hand receiveHand(int id) {
		UserPrompts.promptHand(id);
		
		Card[] cards = new Card[5];
		for (int i = 0; i < 5; i++) {
			cards[i] = receiveCard((i +1));
		}
		
		Hand hand;
		try {
			hand = new Hand(id, cards);
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
			hand = receiveHand(id);
		}
		return hand;
	}
	
	private static Card receiveCard(int number) {
		UserPrompts.promptCard(number);
		
		String input = scanner.next();
		char s = input.charAt(0);
		char v = input.charAt(1);
		
		Card newCard;
		try {
			newCard = createCard(s,v);
		} 
		catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			UserPrompts.giveInstructions();
			newCard = receiveCard(number);
		}
		
		return newCard;
	}	
	
	private static void exit() {
		UserPrompts.promptExit();
		scanner.next();
		scanner.close();
		System.exit(0);
	}
	
	private static Card createCard(char suit, char value) throws IllegalArgumentException {
		Suit s = determineSuit(suit);
		int v = determineValue(value);
		return new Card(s, v);
	}
	
	private static Suit determineSuit(char s ) throws IllegalArgumentException {
		s = Character.toLowerCase(s);
		switch (s) {
		case 'd': return Suit.DIAMONDS; 
		case 'h': return Suit.HEARTS;
		case 's': return Suit.SPADES;
		case 'c': return Suit.CLUBS;
		default: throw new IllegalArgumentException("Illegal Suit");
		}
	}
	
	private static int determineValue(char v) throws IllegalArgumentException {
		v = Character.toLowerCase(v);
		switch (v) {
		case 't': return 10;
		case 'j': return 11;
		case 'q': return 12;
		case 'k': return 13;
		case 'a': return 14;
		default:
			int value = Character.getNumericValue(v);
			if (value >= 2 && value <= 14) {
				return value;
			}
			else {
				throw new IllegalArgumentException("Illegal Value");
			}
		}
	}

}
