package main.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Hand {
	
	public final int HAND_SIZE = 5;
	private HandType handType;
	private int id;
	private Card [] cards;
	private HandBuilder builder;
	
	public Hand(int id, Card[] newCards) throws Exception {
		if (newCards.length == HAND_SIZE) {
			builder = new HandBuilder();
			this.id = id;
			cards = newCards;
			builder.buildHand(newCards);
		}
		else {
			throw new Exception();
		}
		
	}
	
	public HandType getHandType() {
		return handType;
	}
	
	public Card[] getCards() {
		return cards;
	}
	
	public int getID() {
		return id;
	}
	
	
	
	class HandBuilder {
		
		boolean straight;
		boolean flush;
		
		ArrayList<Card> cardsBeingProcessed;
		ArrayList<Integer> valueComposition;
		ArrayList<Integer> valueCount;
		//valueCount[i] informs how often the card value at valueComposition[i] occurs in the hand.
		
		void buildHand(Card [] newCards) {
			cardsBeingProcessed = new ArrayList<Card>(Arrays.asList(newCards));
			handType = determineHandType();
			ArrayList<Card> orderedCards = sortCardsAccordingToHandType();
			cards = orderedCards.toArray(new Card[HAND_SIZE]);
		}
		
		
		HandType determineHandType() {
			straight = isStraight();
			flush = isFlush();
			
			if (flush && straight) {
				return HandType.STRAIGHT_FLUSH;
			}
			
			determineValueComposition();
			
			if (valueComposition.size() == 2) {
				determineValueCount();
				if (valueCount.contains(new Integer(4))) {
					return HandType.FOUR_OF_A_KIND;
				}
				else {
					return HandType.FULL_HOUSE;
				}
			}
			else if (flush) {
				return HandType.FLUSH;
			}
			else if (straight) {
				return HandType.STRAIGHT;
			}
			else if (valueComposition.size() == 3) {
				determineValueCount();
				if(valueCount.contains(new Integer(3))) {
					return HandType.THREE_OF_A_KIND;
				}
				else {
					return HandType.TWO_PAIRS;
				}
			}
			else if (valueComposition.size() == 4) {
				return HandType.PAIR;
			}
			
			return HandType.HIGH_CARD;
						
		}
			
		
		void determineValueComposition() {
			valueComposition = new ArrayList<Integer>();
			
			for (int i = 0; i < cardsBeingProcessed.size(); i++) {
				Integer currentCardValue = new Integer(cardsBeingProcessed.get(i).getValue());
				if(!valueComposition.contains(currentCardValue)) {
					valueComposition.add(currentCardValue);
				}				
			}
		}
		
		void determineValueCount() {
			valueCount = new ArrayList<Integer>();
			
			for (int i = 0; i < valueComposition.size(); i++) {
				valueCount.add(new Integer(0));
			}
			
			for (int i = 0; i < cardsBeingProcessed.size(); i++) {
				Integer currentCardValue = new Integer(cardsBeingProcessed.get(i).getValue());
				
				for (int j = 0; j < valueComposition.size(); j++) {
					if (currentCardValue.equals(valueComposition.get(j))) {
						valueCount.set(j, (valueCount.get(j) + 1));
					}
				}
			}
		}
		
		
		ArrayList<Card> sortCardsAccordingToHandType() {
			ArrayList<Card> orderedCards = new ArrayList<Card>();
			
			if (handType == HandType.FOUR_OF_A_KIND) {
				orderedCards = sortFourOfAKind();
			}
			else if (handType == HandType.TWO_PAIRS) {
				orderedCards = sortTwoPairs();
			}
			else if (handType == HandType.FULL_HOUSE) {
				orderedCards = sortFullHouse();
			}
			else if (handType == HandType.THREE_OF_A_KIND) {
				orderedCards = sortThreeOfAKind();
			}
			else if (handType == HandType.PAIR) {
				orderedCards = sortPair();
			}
			else { //High_Card, Straight, Flush, or Straight Flush
				CardComparator comparator = new CardComparator();
				Collections.sort(cardsBeingProcessed, Collections.reverseOrder(comparator));
			}
			
			return orderedCards;
		}
		
		ArrayList<Card> sortFourOfAKind() {
			Integer valueOfQuadruple = valueComposition.get(valueCount.indexOf(new Integer(4)));
			
			ArrayList<Card> orderedCards = removeAndReturnAllWithValue(valueOfQuadruple.intValue());
			orderedCards.addAll(cardsBeingProcessed);
			
			return orderedCards;
			
		}
		
		ArrayList<Card> sortFullHouse() {
			Integer valueOfPair = valueComposition.get(valueCount.indexOf(new Integer(2)));
			ArrayList<Card> pair = removeAndReturnAllWithValue(valueOfPair);
			
			cardsBeingProcessed.addAll(pair);
			return cardsBeingProcessed;
		}
		
		ArrayList<Card> sortPair() {
			determineValueCount();
			Integer valueIndex = valueCount.indexOf(new Integer(2));
			Integer valueOfPair = valueComposition.get(valueIndex.intValue());
			
			ArrayList<Card> orderedCards = removeAndReturnAllWithValue(valueOfPair.intValue());
			
			CardComparator comparator = new CardComparator();
			Collections.sort(cardsBeingProcessed, Collections.reverseOrder(comparator));
			
			orderedCards.addAll(cardsBeingProcessed);
			
			return orderedCards;
		}
		
		ArrayList<Card> sortTwoPairs() {
			Integer valueOfSingleCard = valueComposition.get(valueCount.indexOf(new Integer(1)));
			
			ArrayList<Card> single = removeAndReturnAllWithValue(valueOfSingleCard.intValue());
			
			CardComparator comparator = new CardComparator();
			Collections.sort(cardsBeingProcessed, Collections.reverseOrder(comparator));		
			
			cardsBeingProcessed.addAll(single);
			return cardsBeingProcessed;			
		}
		
		ArrayList<Card> sortThreeOfAKind() {
			Integer valueOfTriple = valueComposition.get(valueCount.indexOf(new Integer(3)));
			ArrayList<Card> orderedCards = removeAndReturnAllWithValue(valueOfTriple.intValue());
			
			CardComparator comparator = new CardComparator();
			Collections.sort(cardsBeingProcessed, Collections.reverseOrder(comparator));
			
			orderedCards.addAll(cardsBeingProcessed);
			
			return orderedCards;
		}
		
		ArrayList<Card> removeAndReturnAllWithValue(int value) {
			ArrayList<Card> cards = new ArrayList<Card>();
			for (int i = 0; i < cardsBeingProcessed.size(); i++) {
				Card currentCard = cardsBeingProcessed.get(i);
				if (currentCard.getValue() == value) {
					cards.add(currentCard);
				}
			}
			
			for (int i = 0; i < cards.size(); i++) {
				cardsBeingProcessed.remove(cards.get(i));
			}
			
			return cards;			
		}
		
		boolean isFlush() {
			for (int i = 1; i < cards.length; i++){
				if (cardsBeingProcessed.get(i).getSuit() != cardsBeingProcessed.get(0).getSuit()) {
					return false;
				}
			}
			return true;
		}
		
		boolean isStraight() {
			boolean straight = true;
			CardComparator comparator = new CardComparator();
			Collections.sort(cardsBeingProcessed, Collections.reverseOrder(comparator));
			
			for (int i = 1; i < cards.length; i++) {
				if (cardsBeingProcessed.get(i-1).getValue() != (cardsBeingProcessed.get(i).getValue() + 1)) {
					straight = false;
				}
			}
			ArrayList<Card> aces = removeAndReturnAllWithValue(14);
			if (! aces.isEmpty() && aces.size() == 1) {
				Card lowAce = new Card(aces.get(0).getSuit(),1);
				cardsBeingProcessed.add(lowAce);
				straight = isStraight();
				
				if (!straight) {
					cardsBeingProcessed.remove(lowAce);
					cardsBeingProcessed.add(aces.get(0));
				}
			}
			else {
				cardsBeingProcessed.addAll(aces);
			}
			
			return straight;
		}		
		
		
		
	}
	
	
	
	

}
