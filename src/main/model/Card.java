package main.model;


public class Card {
	
	private Suit suit;
	private int value;
	
	public Card(Suit s, int v) throws IllegalArgumentException {
		suit = s;
		if (isValidCardValue(v)) {
			value = v;
		}
	}
	
	private boolean isValidCardValue(int v) throws IllegalArgumentException {
		if (v <= 14 && v >= 1) {
			return true;
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	
	public Suit getSuit() {
		return this.suit;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public void setValue(int v) {
		
	}
	
}
