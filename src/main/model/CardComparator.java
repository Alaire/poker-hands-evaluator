package main.model;

import java.util.Comparator;

public class CardComparator implements Comparator<Card> {

	@Override
	public int compare(Card card1, Card card2) {
		int value1 = card1.getValue();
		int value2 = card2.getValue();
		
		if(value1 > value2) {
			return 1;
		}
		else if (value1 == value2) {
			return 0;
		}
		else {
			return -1;
		}
	}

}
