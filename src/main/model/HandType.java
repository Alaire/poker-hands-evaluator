package main.model;

/**
 * An enum for hand types was used to increase readability elsewhere.
 * However, each has been assigned a relative integer value for ranking purposes, 
 * in order to preserve the practical meaning of the different types of hands and make comparisons between them easier.
 * @author Ayme Barclay
 */
public enum HandType {
	HIGH_CARD(0), PAIR(1), TWO_PAIRS(2), THREE_OF_A_KIND(3), STRAIGHT(4), FLUSH(5), FULL_HOUSE(6), FOUR_OF_A_KIND(7), STRAIGHT_FLUSH(8);
	
	private int handTypeRank;
	
	private HandType(int rank) {
		handTypeRank = rank;
	}
	
	public int getHandTypeRank() {
		return handTypeRank;
	}

}
