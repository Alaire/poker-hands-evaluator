package main.model;

import java.util.Comparator;

public class HandComparator implements Comparator<Hand> {
	
	@Override
	public int compare(Hand hand1, Hand hand2) {
		int hand1Rank = hand1.getHandType().getHandTypeRank();
		int hand2Rank = hand2.getHandType().getHandTypeRank();
		
		if (hand1Rank > hand2Rank) {
			return 1;
		}
		else if (hand1Rank < hand2Rank) {
			return -1;
		}
		else {
			Card[] hand1Cards = hand1.getCards();
			Card[] hand2Cards = hand2.getCards();
			
			for (int i = 0; i < 5; i++) {
				if (hand1Cards[i].getValue() > hand2Cards[i].getValue()) {
					return 1;
				}
				else if (hand1Cards[i].getValue() < hand2Cards[i].getValue()) {
					return -1;
				}
				
			}
			
			return 0;
			
		}
	}

}
