package main;

public class UserPrompts {
	
	private static final String WELCOME = "Welcome.";
	
	private static final String INSTRUCTIONS = "When prompted, please enter each card for each hand. \n"
			+ "Cards are input in the following format: suit, consisting of a single letter \n"
			+ "(c for clubs, d for diamonds, h for hearts, s for spades),\n followed by a character "
			+ "denoting the type of card \n (2, 3, 4, 5, 6, 7, 8, 9, T for 10, J for jack, Q for queen, K for king, A for ace).\n"
			+ "Please follow each card with an 'Enter'.";
	
	
	private static final String HAND = "Hand";
	
	private static final String TIE = "The two hands are equal.";
	private static final String WIN = " wins.";
	private static final String CARD = "Card";
	private static final String EXIT = "Press Any Key To Exit.";
	
	public static void promptExit() {
		System.out.println(EXIT);
	}
	
	public static void welcome() {
		System.out.println(WELCOME);
	}
	
	public static void giveInstructions() {
		System.out.println(INSTRUCTIONS);
	}
	
	public static void announceTie() {
		System.out.println(TIE);
	}
	
	public static void announceWinner(int id) {
		System.out.println(id + WIN);
	}
	
	public static void promptHand(int id) {
		System.out.println(HAND  + " " + id + ":");
	}
	
	public static void promptCard(int number) {
		System.out.println(CARD + " " + number + ":");
	}

}
